internal class Program
{
    public static async Task Main(string[] args)
    {
        IClient client = new Client();
        var ids = Enumerable.Range(1, 18)
            .Select(x => (long)x).ToArray();

        await client.GetArrayAsync(ids, 3);

        Console.ReadKey();
    }
}

public class Response { }

public interface IClient
{
    Task<Response> GetAsync(long id);
}

public class Client : IClient
{
    public async Task<Response> GetAsync(long id)
    {
        await Task.Delay(3000);
        Console.WriteLine($"Thread: {Thread.CurrentThread.ManagedThreadId}. " +
                          $"Task number: {id}. " +
                          $"Time: {DateTime.UtcNow}");

        return new Response();
    }
}

public static class ClientExtensions
{
    public static async Task<Response[]> GetArrayAsync(this IClient client, long[] ids, int maxDegreeOfParallelism)
    {
        SemaphoreSlim s = new SemaphoreSlim(maxDegreeOfParallelism, maxDegreeOfParallelism);

        var tasks = ids.Select(async x =>
        {
            await s.WaitAsync();

            try
            {
                return await client.GetAsync(ids[x - 1]);
            }
            finally
            {
                s.Release();
            }
        }).ToArray();

        return await Task.WhenAll(tasks);
    }
}
